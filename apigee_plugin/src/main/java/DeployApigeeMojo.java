import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import za.co.mtn.apigee.ApigeeUtility;

@Mojo( name = "deployproxies")
public class DeployApigeeMojo extends AbstractMojo
{
	@Parameter(defaultValue="") 
	private String resourcePath;

	public void execute() throws MojoExecutionException
	{
		String org = System.getenv("APIGEE_ORG");
		String email = System.getenv("APIGEE_EMAIL");
		String pass = System.getenv("APIGEE_PASSWORD");
		String token = "";
		try {
			token = ApigeeUtility.getToken(org,email,pass);
		}
		catch(Exception e) {
			getLog().error( "Unable to login into Apigee. Please check Apigee email, password and organisation have been set as environmental variables" );
			throw new MojoExecutionException("Build failed");
		} 

		File[] environments = envFinder( resourcePath+"/proxies");
		for(File environment : environments){
			
			String envName = environment.getName();

			File[] zipFiles = finder( resourcePath+"/proxies/"+envName);
			for(File zip : zipFiles){
				String filename = zip.getAbsolutePath();
				String[] parts = zip.getName().split("_rev");
				if(parts.equals(null) || parts.length <= 1) {
					getLog().error( "The zip file "+zip.getName()+" is incorrectly named and cannot be deployed" );
					continue;
				}
				String name = parts[0];
				try {
					ApigeeUtility.main(new String[] { "proxy", "import", "-n", name, "-l", filename, "-t", token });
				}
				catch(Exception e) {
					getLog().error( "The zip file "+zip.getName()+" could not be imported and the build has failed" );
					throw new MojoExecutionException(zip.getName() + "failed");
				} 

				try {
					String[] revisions = ApigeeUtility.getLatestRevision(name, token);
					int numRev = revisions.length;
					for (int i = 0 ; i < numRev - 1 ; i++) {
						try {
							ApigeeUtility.main(new String[] { "proxy", "undeploy", "-n", name, "-r", revisions[i], "-e", envName,"-t", token });
						}
						catch(Exception e) {
							getLog().info( "Revision has not been deployed" );
						} 
					}

					ApigeeUtility.main(new String[] { "proxy", "deploy", "-n", name, "-r", revisions[numRev -1], "-e", envName,"-t", token });

				}
				catch(Exception e) {
					getLog().error( "The zip file "+zip.getName()+" could not be deployed and the build has failed" );
					throw new MojoExecutionException(zip.getName() + "failed");
				} 
			}
		}
	}

	public File[] finder( String dirName){
		File dir = new File(dirName);
		return dir.listFiles(new FilenameFilter() { 
			public boolean accept(File dir, String filename)
			{ return filename.endsWith(".zip"); }
		} );

	}

	public static File[] envFinder( String dirName){
		File dir = new File(dirName);
		return dir.listFiles();
	}

}