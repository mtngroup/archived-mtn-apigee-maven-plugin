package za.co.mtn.apigee.commands.proxy;

import picocli.CommandLine;

@CommandLine.Command(
        name = "proxy",
        description = "proxy action to be performed",
        subcommands = {
                Import.class,
                Export.class,
                Deploy.class,
                Undeploy.class,
                Delete.class,
                Revisions.class
        })
public class Proxy implements Runnable {

    @Override
    public void run() {
        CommandLine.usage(this, System.out);
    }

}
