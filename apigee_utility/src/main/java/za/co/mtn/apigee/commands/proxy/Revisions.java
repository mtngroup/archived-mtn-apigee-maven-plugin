package za.co.mtn.apigee.commands.proxy;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import picocli.CommandLine;
import za.co.mtn.apigee.commands.Command;
import za.co.mtn.apigee.helpers.CommandHelper;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@CommandLine.Command(name = "revisions", description = "get the revisions of an Apigee API Proxy")
public class Revisions extends Command implements Runnable {

    @CommandLine.Option(names = {"-n", "--name"}, description = "name of the proxy to deploy", required = true)
    private String name;

    @CommandLine.Option(names = {"-t", "--token"}, description = "token to authenticate the request", required = true)
    private String token;

    @Override
    public void run() {
        String endpoint = CommandHelper.retrieveEndpoint(this, token);
        String trimmedToken = CommandHelper.extractToken(token);

        HttpClient httpClient = CommandHelper.createClient();
        if (host != null) {
            httpClient = CommandHelper.createClient(host, port);
        }

        try {
    		URIBuilder uriBuilder = new URIBuilder(CommandHelper.encodeURL(endpoint + "/apis/" + name
                    + "/revisions"));
    		URI revisionsUri = uriBuilder.build();

            HttpGet httpGet = new HttpGet(revisionsUri);
            httpGet.setHeader("Authorization", "Basic " + trimmedToken);

            System.out.println("SENDING PROXY REVISIONS REQUEST");
            System.out.println("=================");
            System.out.println("Endpoint URL: " + httpGet.getMethod() + " " + revisionsUri.toString());
            System.out.println();
            System.out.println();

            HttpResponse httpResponse = httpClient.execute(httpGet);
            int code = httpResponse.getStatusLine().getStatusCode();
            String response = EntityUtils.toString(httpResponse.getEntity());

            if (code == 404) {
                if (response.contains("messaging.config.beans.ApplicationDoesNotExist")) {
                    throw new IllegalArgumentException("An APIProxy named " + name + " does not exist in organization");
                }
            } else if (code == 401) {
                throw new AuthenticationException("The token supplied is invalid.");
            }

            System.out.println("RECEIVED PROXY REVISIONS RESPONSE");
            System.out.println("=================");
            System.out.println("SUCCESS");
            System.out.println(response);
        } catch (URISyntaxException | IOException | AuthenticationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    
    
    public String[] getLatestRevision(String name, String tokenKey) {
        String endpoint = CommandHelper.retrieveEndpoint(this, tokenKey);
        String trimmedToken = CommandHelper.extractToken(tokenKey);

        HttpClient httpClient = CommandHelper.createClient();
        if (host != null) {
            httpClient = CommandHelper.createClient(host, port);
        }

        try {
    		URIBuilder uriBuilder = new URIBuilder(CommandHelper.encodeURL(endpoint + "/apis/" + name
                    + "/revisions"));
    		URI revisionsUri = uriBuilder.build();

            HttpGet httpGet = new HttpGet(revisionsUri);
            httpGet.setHeader("Authorization", "Basic " + trimmedToken);


            HttpResponse httpResponse = httpClient.execute(httpGet);
            int code = httpResponse.getStatusLine().getStatusCode();
            String response = EntityUtils.toString(httpResponse.getEntity());
            
            ObjectMapper mapper = new ObjectMapper();
            String [] contents = mapper.readValue(response, String[].class);
            
            
            if (code == 404) {
                if (response.contains("messaging.config.beans.ApplicationDoesNotExist")) {
                    throw new IllegalArgumentException("An APIProxy named " + name + " does not exist in organization");
                }
            } else if (code == 401) {
                throw new AuthenticationException("The token supplied is invalid.");
            }
            

            	
            return contents;


        } catch (URISyntaxException | IOException | AuthenticationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
