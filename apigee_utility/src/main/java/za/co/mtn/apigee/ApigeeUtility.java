package za.co.mtn.apigee;

import picocli.CommandLine;
import za.co.mtn.apigee.commands.Login;
import za.co.mtn.apigee.commands.app.App;
import za.co.mtn.apigee.commands.developer.Developer;
import za.co.mtn.apigee.commands.product.Product;
import za.co.mtn.apigee.commands.proxy.Proxy;
import za.co.mtn.apigee.commands.proxy.Revisions;

@CommandLine.Command(
        name = "action", version = "v0.0.1",
        description = "action to be performed",
        mixinStandardHelpOptions = true,
        subcommands = {
                Login.class,
                Proxy.class,
                Product.class,
                App.class,
                Developer.class
        })
public class ApigeeUtility implements Runnable {

    @Override
    public void run() {
        CommandLine.usage(this, System.out);
    }

    public static void main(String[] args) {
        CommandLine.run(new ApigeeUtility(), args);
    }
    
    public static String getToken(String org, String email, String pass) {

    	Login login = new Login();
		String token = login.getToken(org, email, pass);

    	return token;
    }
    
    public static String[] getLatestRevision(String name, String token) {

    	Revisions revisions = new Revisions();
		String[] latestRevision = revisions.getLatestRevision(name,token);

    	return latestRevision;
    }

}
