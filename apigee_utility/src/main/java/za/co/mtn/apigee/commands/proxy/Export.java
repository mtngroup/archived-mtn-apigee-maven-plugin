package za.co.mtn.apigee.commands.proxy;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.EntityUtils;
import picocli.CommandLine;
import za.co.mtn.apigee.commands.Command;
import za.co.mtn.apigee.helpers.CommandHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@CommandLine.Command(name = "export", description = "export an Apigee API Proxy into a ZIP file")
public class Export extends Command implements Runnable {

    @CommandLine.Option(names = {"-l", "--location"}, description = "location to place the exported proxies")
    private String location;

    @CommandLine.Option(names = {"-t", "--token"}, description = "token to authenticate the request", required = true)
    private String token;

    @Override
    public void run() {
        String endpoint = CommandHelper.retrieveEndpoint(this, token);
        String trimmedToken = CommandHelper.extractToken(token);

        System.out.println("Export a list of proxies.");

        List<String> proxies = retrieveProxies(endpoint, trimmedToken);

        proxies.forEach(proxy -> {
            System.out.println("Finding latest revision for proxy: " + proxy);
            String latestRevision = findLatestRevision(endpoint, trimmedToken, proxy);
            System.out.println("Latest revision for proxy: " + latestRevision);

            System.out.println("Exporting proxy: " + proxy);
            exportProxy(endpoint, trimmedToken, proxy, latestRevision);
        });
    }

    private void exportProxy(String endpoint, String token, String proxy, String revision) {
        String newEndpoint = endpoint + "/" + proxy + "/revisions/" + revision + "?format=bundle";

        try {
            HttpClient httpClient = CommandHelper.createClient();
            if (host != null) {
                httpClient = CommandHelper.createClient(host, port);
            }

            URIBuilder uriBuilder = new URIBuilder(newEndpoint);
            URI exportUri = uriBuilder.build();

            HttpGet httpGet = new HttpGet(exportUri);
            httpGet.setHeader("Authorization", "Basic " + token);

            System.out.println("SENDING EXPORT PROXY REQUEST");
            System.out.println("=================");
            System.out.println("Endpoint URL: " + httpGet.getMethod() + " " + exportUri.toString());

            System.out.println();
            System.out.println();

            HttpResponse httpResponse = httpClient.execute(httpGet);
            int code = httpResponse.getStatusLine().getStatusCode();

            if (code == 401) {
                throw new AuthenticationException("The token supplied is invalid.");
            }

            System.out.println("RECEIVED EXPORT PROXY RESPONSE");
            System.out.println("=================");
            System.out.println("SUCCESS");

            byte[] response = EntityUtils.toByteArray(httpResponse.getEntity());

            String locationToUse = System.getProperty("user.dir");
            if (location != null) {
                locationToUse = location;
            }

            String proxiesDirectory = locationToUse + "/proxies";
            File directory = new File(proxiesDirectory);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            File file = new File(proxiesDirectory + "/" + proxy + ".zip");
            try (OutputStream fileOuputStream = new FileOutputStream(file)) {
                fileOuputStream.write(response);
                System.out.println("ZIP file created.");
            }
        } catch (IOException | URISyntaxException | AuthenticationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private String findLatestRevision(String endpoint, String token, String proxy) {
        String newEndpoint = endpoint + "/" + proxy + "/revisions";

        try {
            HttpClient httpClient = CommandHelper.createClient();
            if (host != null) {
                httpClient = CommandHelper.createClient(host, port);
            }

            URIBuilder uriBuilder = new URIBuilder(newEndpoint);
            URI exportUri = uriBuilder.build();

            HttpGet httpGet = new HttpGet(exportUri);
            httpGet.setHeader("Authorization", "Basic " + token);

            System.out.println("SENDING PROXY REVISION LIST REQUEST");
            System.out.println("=================");
            System.out.println("Endpoint URL: " + httpGet.getMethod() + " " + exportUri.toString());

            System.out.println();
            System.out.println();

            HttpResponse httpResponse = httpClient.execute(httpGet);
            int code = httpResponse.getStatusLine().getStatusCode();

            if (code == 401) {
                throw new AuthenticationException("The token supplied is invalid.");
            }

            System.out.println("RECEIVED PROXY REVISION LIST RESPONSE");
            System.out.println("=================");
            System.out.println("SUCCESS");

            String response = EntityUtils.toString(httpResponse.getEntity());
            System.out.println(response);

            ObjectMapper objectMapper = new ObjectMapper();
            List<String> revisions = objectMapper.readValue(response, objectMapper.getTypeFactory().constructCollectionType(List.class, String.class));

            return revisions.get(revisions.size() - 1);
        } catch (IOException | URISyntaxException | AuthenticationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private List<String> retrieveProxies(String endpoint, String trimmedToken) {
        try {
            HttpClient httpClient = CommandHelper.createClient();
            if (host != null) {
                httpClient = CommandHelper.createClient(host, port);
            }

            URIBuilder uriBuilder = new URIBuilder(endpoint);
            URI exportUri = uriBuilder.build();

            HttpGet httpGet = new HttpGet(exportUri);
            httpGet.setHeader("Authorization", "Basic " + trimmedToken);

            System.out.println("SENDING PROXY LIST REQUEST");
            System.out.println("=================");
            System.out.println("Endpoint URL: " + httpGet.getMethod() + " " + exportUri.toString());

            System.out.println();
            System.out.println();

            HttpResponse httpResponse = httpClient.execute(httpGet);
            int code = httpResponse.getStatusLine().getStatusCode();

            if (code == 401) {
                throw new AuthenticationException("The token supplied is invalid.");
            }

            System.out.println("RECEIVED PROXY LIST RESPONSE");
            System.out.println("=================");
            System.out.println("SUCCESS");

            String response = EntityUtils.toString(httpResponse.getEntity());
            System.out.println(response);

            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(response, objectMapper.getTypeFactory().constructCollectionType(List.class, String.class));
        } catch (IOException | URISyntaxException | AuthenticationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
