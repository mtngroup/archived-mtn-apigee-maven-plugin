**MADAPI Maven Plugin for Apigee Proxy Deployments**

This maven plug can be used to automatically deploy proxies to the MTN Apigee platform

---

## Setup Maven Plug

1. Add this plugin as a plugin to the project's POM:

	    </plugins>
		    <plugin>
		  	    <groupId>org.bitbucket.mtngroup</groupId>
				    <artifactId>apigee-maven-plugin</artifactId>
				    <version>1.0-SNAPSHOT</version>
				    <configuration>
				    <resourcePath>${project.basedir}/</resourcePath>
				    </configuration>
				    <executions>
                        <execution>
                    	    <phase>package</phase>
                            <goals>
                                <goal>deployproxies</goal>
                            </goals>
                        </execution>
                    </executions>
		       </plugin>
        </plugins>
	
2. Add this the plugin repo to project's POM:


	    <pluginRepositories>
    	    <pluginRepository>
		    <id>nexus-snapshots</id>
	        <snapshots>
	          <enabled>true</enabled>
	        </snapshots>
		    <url>http://${env.OPENSHIFT_NEXUS_URL}/nexus/content/repositories/mtn-snapshots/</url>
		    </pluginRepository>
         </pluginRepositories>


		
## Add Proxies

1. Create a new folder called "proxies" in the same directory level as the project's POM
2. Add the proxies (in the form of a ZIP file) to this new "proxies" folder. Conventional ZIP name should be:

	<Proxy Name>_rev<Revision Number>_<Date>.zip

Note: More than one proxy can be added to a project

## Setup the Apigee Login Process

In order for the proxies to be created on Apigee, valid login details and organisation must be provided. This is achieved by adding credentials the environmental variables as follows:

	Need to set following variables
	
	1. APIGEE_ORG = "<Apigee Organisation>"
	2. APIGEE_EMAIL = "<Apigee Email>"
	3. APIGEE_PASSWORD = "<Apigee Password>"
	
	Need to set following variables for Nexus

	1. NEXUS_USERNAME = "<Nexus_USERNAME>"
	2. NEXUS_PASSWORD = "<Nexus_PASSWORD>"
	3. OPENSHIFT_NEXUS_URL = "<Nexus_OPENSHIFT_NEXUS_URL>" (ie "nexus-mtnapitest.e4ff.pro-eu-west-1.openshiftapps.com")
	
	For Windows, this is achieved with following commands in terminal:
	
		setx APIGEE_ORG "<Apigee Organisation>"
		setx APIGEE_EMAIL "<Apigee Email>"
		setx APIGEE_PASSWORD "<Apigee Password>"
		
		setx NEXUS_USERNAME "<Nexus_USERNAME>"
		setx NEXUS_PASSWORD "<Nexus_PASSWORD>"
		setx OPENSHIFT_NEXUS_URL "nexus-mtnapitest.e4ff.pro-eu-west-1.openshiftapps.com" (This could change)
		
	Note: Your IDE or Terminal maybe need to be restarted in order for these variables to be set.

#Setup Build Pipeline

	1. The project's "bitbucket-pipelines.yml" will need to be edited.
	
		1.1 The plug goal should be added 
		
					mvn complie apigee-maven-plugin:1.0-SNAPSHOT:deployproxies 
	
		1.2 The setting's option must have settings.xml set:
		
					mvn -s settings.xml compile 
					
	2. The setting's file (with nexus server info) must be added to the root of the project:
	
			<servers>
				<server>
				  <id>nexus-snapshots</id>
				  <username>${env.NEXUS_USERNAME}</username>
				  <password>${env.NEXUS_PASSWORD}</password>
				</server>
			</servers>
			
			
#Deploying the Plugin to Nexus

	1. cd into the root of the apigee_plugin project (where pom.xml and settings.xml is stored)
	2. Run following maven deploy command:
			mvn -s settings.xml deploy